import argparse
from inference.config import *
import sys
sys.path.append('/home/jupyter/notebooks/src')

from utils.data_generator import Data_Generator
from utils.vocabulary import Vocabulary
from models.siamese_dense_conv import Model
import keras
import numpy as np
import os
from sklearn.metrics import roc_auc_score,roc_curve,confusion_matrix,f1_score,accuracy_score

CWD = os.getcwd()

def get_args():
    argparser = argparse.ArgumentParser(description=__doc__)
    argparser.add_argument(
        '-c', '--config',
        metavar='C',
        default='None',
        help='The Configuration json file for evaluation')
    args = argparser.parse_args()
    return args

def setup_model(config):
    vocab = Vocabulary()
    vocab.load(os.path.join(CWD,config.vocab_path))
    
    model = Model(vocab)
    model.load_weights(os.path.join(CWD,config.model_path))
    
    return model,vocab

def get_predictions(model,data_gen):
    
    print('Extracting predictions')
    eval_batches = data_gen.gen_evaluation_data()
    print('Number of Evaluation Batches',len(eval_batches))
    prediction_probs = []
    true_labels = []
    for index,batch in enumerate(eval_batches):
        prediction = model.predict(batch[0],batch[1])
        prediction_probs.append(prediction)
        true_labels.append(batch[2])

    prediction_probs = np.stack(prediction_probs,axis=0).reshape((-1,1))
    true_labels = np.stack(true_labels,axis=0).reshape((-1,1))
    
    return prediction_probs,true_labels

def compute_statistics(prediction_probs,true_labels):
    print('Computing Accuracy and F1 Score for different thresholds in range(0.1,0.9)')
    print()
    
    accuracies = []
    
    for threshold in np.arange(0.1,0.9,0.05):
        print('Threshold:',threshold)
        predicted_classes = (prediction_probs[:,0] > threshold).astype(np.int)

        tn, fp, fn, tp = confusion_matrix(true_labels,predicted_classes).ravel()

        f1 = f1_score(true_labels,predicted_classes)
        acc = accuracy_score(true_labels,predicted_classes)
        accuracies.append([acc,threshold])
        
        print('F1 score:',f1)
        print('Accuracy:',acc)
        print('---------------')
        
    accuracies = sorted(accuracies,key=lambda x: x[0],reverse=True)
    print('\nFound best accuracy of {0} at threshold of {1}\n'.format(accuracies[0][0],accuracies[0][1]))
    
def compute_accuracy(prediction_probs,true_labels,threshold):
    
    predicted_classes = (prediction_probs[:,0] > threshold).astype(np.int)

    acc = accuracy_score(true_labels,predicted_classes)
    
    print('Accuracy:',acc)
    print('---------------')

def main(args):
    
    config = process_config(args.config)
    
    model,vocab = setup_model(config)
    
    data_gen = Data_Generator(vocab,1,None)
    data_gen.bulk_load(os.path.join(CWD,config.evaluation_csv))
    
    prediction_probs,true_labels = get_predictions(model,data_gen)
    
    if config.threshold == 0:
        compute_statistics(prediction_probs,true_labels)
    else:
        compute_accuracy(prediction_probs,true_labels,config.threshold)
    
if __name__ == '__main__':
    args = get_args()
    main(args)