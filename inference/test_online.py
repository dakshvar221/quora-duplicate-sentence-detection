import argparse
from inference.config import *
import sys
sys.path.append('/home/jupyter/notebooks/src')

from utils.data_generator import Data_Generator
from utils.vocabulary import Vocabulary
from models.siamese_dense_conv import Model
import keras
import numpy as np
import os
from sklearn.metrics import roc_auc_score,roc_curve,confusion_matrix,f1_score,accuracy_score

CWD = os.getcwd()

def get_args():
    argparser = argparse.ArgumentParser(description=__doc__)
    argparser.add_argument(
        '-c', '--config',
        metavar='C',
        default='None',
        help='The Configuration json file for model loading')
    args = argparser.parse_args()
    return args

def setup_model(config):
    vocab = Vocabulary()
    vocab.load(os.path.join(CWD,config.vocab_path))
    
    model = Model(vocab)
    model.load_weights(os.path.join(CWD,config.model_path))
    
    return model,vocab

def main(args):
    
    config = process_config(args.config)
    model,vocab = setup_model(config)
    
    data_gen = Data_Generator(vocab,1,None)
    
    sentence_1 = input("Input Sentence 1:")
    sentence_2 = input("Input Sentence 2:")
    
    q1_seq = data_gen.text_to_sequence(sentence_1)
    q2_seq = data_gen.text_to_sequence(sentence_2)

    prob = model.predict(q1_seq,q2_seq)[0][0]
    
    print('--------------------------')
    if prob > config.threshold:
        print('Both sentences convey same meaning.')
    else:
        print('Both sentences differ in their meaning.')
    print('--------------------------')
if __name__ == '__main__':
    args = get_args()
    main(args)