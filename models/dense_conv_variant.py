import numpy as np
import keras
import sys
sys.path.append('/home/jupyter/notebooks/src')

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM, GRU
from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.layers import Merge
from keras.layers import TimeDistributed, Lambda
from keras.layers import Convolution1D, GlobalMaxPooling1D
from keras.callbacks import ModelCheckpoint
from keras import backend as K
from keras.layers.advanced_activations import PReLU

class Model:
    
    def __init__(self,vocab):
        self.vocab = vocab
        self.model = self.build_model()
        
    def build_model(self):
        
        model1 = Sequential()
        model1.add(Embedding(self.vocab.word_embedding_matrix.shape[0], self.vocab.word_embedding_matrix.shape[1], embeddings_initializer=keras.initializers.constant(self.vocab.word_embedding_matrix), trainable=False))

        model1.add(TimeDistributed(Dense(300, activation='relu')))
        model1.add(Lambda(lambda x: K.sum(x, axis=1), output_shape=(300,)))

        model2 = Sequential()
        model2.add(Embedding(self.vocab.word_embedding_matrix.shape[0], self.vocab.word_embedding_matrix.shape[1], embeddings_initializer=keras.initializers.constant(self.vocab.word_embedding_matrix), trainable=False))

        model2.add(TimeDistributed(Dense(300, activation='relu')))
        model2.add(Lambda(lambda x: K.sum(x, axis=1), output_shape=(300,)))

        model3 = Sequential()
        model3.add(Embedding(self.vocab.word_embedding_matrix.shape[0], self.vocab.word_embedding_matrix.shape[1], embeddings_initializer=keras.initializers.constant(self.vocab.word_embedding_matrix), trainable=False))
                   
        model3.add(Convolution1D(nb_filter=64,
                                 filter_length=5,
                                 border_mode='valid',
                                 activation='relu',
                                 subsample_length=1))
        model3.add(Dropout(0.2))

        model3.add(Convolution1D(nb_filter=64,
                                 filter_length=5,
                                 border_mode='valid',
                                 activation='relu',
                                 subsample_length=1))

        model3.add(GlobalMaxPooling1D())
        model3.add(Dropout(0.2))

        model3.add(Dense(300))
        model3.add(Dropout(0.2))
        model3.add(BatchNormalization())

        model4 = Sequential()
        model4.add(Embedding(self.vocab.word_embedding_matrix.shape[0], self.vocab.word_embedding_matrix.shape[1], embeddings_initializer=keras.initializers.constant(self.vocab.word_embedding_matrix), trainable=False))
                   
        model4.add(Convolution1D(nb_filter=64,
                                 filter_length=5,
                                 border_mode='valid',
                                 activation='relu',
                                 subsample_length=1))
        model4.add(Dropout(0.2))

        model4.add(Convolution1D(nb_filter=64,
                                 filter_length=5,
                                 border_mode='valid',
                                 activation='relu',
                                 subsample_length=1))

        model4.add(GlobalMaxPooling1D())
        model4.add(Dropout(0.2))

        model4.add(Dense(300))
        model4.add(Dropout(0.2))
        model4.add(BatchNormalization())
        
        merged_model = Sequential()
        merged_model.add(Merge([model1, model2, model3, model4], mode='concat'))
        merged_model.add(BatchNormalization())

        merged_model.add(Dense(300))
        merged_model.add(PReLU())
        merged_model.add(Dropout(0.2))
        merged_model.add(BatchNormalization())

        merged_model.add(Dense(300))
        merged_model.add(PReLU())
        merged_model.add(Dropout(0.2))
        merged_model.add(BatchNormalization())

        merged_model.add(Dense(300))
        merged_model.add(PReLU())
        merged_model.add(Dropout(0.2))
        merged_model.add(BatchNormalization())

        merged_model.add(Dense(300))
        merged_model.add(PReLU())
        merged_model.add(Dropout(0.2))
        merged_model.add(BatchNormalization())

        merged_model.add(Dense(300))
        merged_model.add(PReLU())
        merged_model.add(Dropout(0.2))
        merged_model.add(BatchNormalization())

        merged_model.add(Dense(1))
        merged_model.add(Activation('sigmoid'))

        merged_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        
        return merged_model
        
    def predict(self,X1,X2):
        return self.model.predict([X1,X2,X1,X2])
    
    def load_weights(self,weight_file):
        self.model.load_weights(weight_file)
        self.model._make_predict_function()
