import numpy as np
import keras
import sys
sys.path.append('/home/jupyter/notebooks/src')

from keras.models import Model as K_Model
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM, GRU
from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.layers import Merge,Input,Concatenate
from keras.layers import TimeDistributed, Lambda
from keras.layers import Convolution1D, GlobalMaxPooling1D
from keras.callbacks import ModelCheckpoint
from keras import backend as K
from keras.layers.advanced_activations import PReLU

class Model:
    
    def __init__(self,vocab):
        self.vocab = vocab
        self.model = self.build_model()
        
    def build_model(self):
        
        question1 = Input(shape=(None,))
        question2 = Input(shape=(None,))
        
        embedder = Embedding(self.vocab.word_embedding_matrix.shape[0], self.vocab.word_embedding_matrix.shape[1], embeddings_initializer=keras.initializers.constant(self.vocab.word_embedding_matrix), trainable=False)
        
        q1_embedded = embedder(question1)
        q2_embedded = embedder(question2)
        
        
        dense_layer = Dense(300,activation='relu')
        
        conv_1_layer = Convolution1D(nb_filter=64,
                                 filter_length=5,
                                 border_mode='valid',
                                 activation='relu',
                                 subsample_length=1)
        
        conv_2_layer = Convolution1D(nb_filter=64,
                                 filter_length=5,
                                 border_mode='valid',
                                 activation='relu',
                                 subsample_length=1)
        
        max_pooler = Lambda(lambda x: K.sum(x,axis=1), output_shape=(300,))
        
        conv_dense = Dense(300)
        
        q1_dense = max_pooler(TimeDistributed(dense_layer)(q1_embedded))
        q2_dense = max_pooler(TimeDistributed(dense_layer)(q2_embedded))
        
        
        
        q1_conv_1 = Dropout(0.2)(conv_1_layer(q1_embedded))
        q1_conv_2 = Dropout(0.2)(GlobalMaxPooling1D()(conv_2_layer(q1_conv_1)))
        q1_conv_feats = conv_dense(q1_conv_2)
        q1_conv_feats = Dropout(0.2)(q1_conv_feats)
        q1_conv_feats = BatchNormalization()(q1_conv_feats)
        
        
        q2_conv_1 = Dropout(0.2)(conv_1_layer(q2_embedded))
        q2_conv_2 = Dropout(0.2)(GlobalMaxPooling1D()(conv_2_layer(q2_conv_1)))
        q2_conv_feats = conv_dense(q2_conv_2)
        q2_conv_feats = Dropout(0.2)(q2_conv_feats)
        q2_conv_feats = BatchNormalization()(q2_conv_feats)
        
        
        merged_feats = Concatenate()([q1_dense,q2_dense,q1_conv_feats,q2_conv_feats])
        
        merged_feats = BatchNormalization()(merged_feats)
        
        fc_1 = BatchNormalization()(Dropout(0.2)(PReLU()(Dense(300)(merged_feats))))
        fc_2 = BatchNormalization()(Dropout(0.2)(PReLU()(Dense(300)(fc_1))))
        fc_3 = BatchNormalization()(Dropout(0.2)(PReLU()(Dense(300)(fc_2))))
        fc_4 = BatchNormalization()(Dropout(0.2)(PReLU()(Dense(300)(fc_3))))
        
        output_prob = Activation('sigmoid')(Dense(1)(fc_4))
        
        final_model = K_Model([question1,question2],output_prob)

        final_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        
        return final_model
        
    def predict(self,X1,X2):
        return self.model.predict([X1,X2])
    
    def load_weights(self,weight_file):
        self.model.load_weights(weight_file)
        self.model._make_predict_function()
