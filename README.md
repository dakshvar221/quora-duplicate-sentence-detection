## Quora Duplicate question detection

### Requirements

1. Tensorflow==1.4.0
2. Keras==2.1.5
3. bunch==1.0.1
4. numpy
5. scikit-learn

### Repo Structure

```
├── data  # All Data files                                                                                                                    
│   ├── cleaned.csv  # Cleaned version of data.csv                                                                               
│   ├── data.csv  # Original data file that was given                                                                                                          
│   ├── eval_config.json                                                                                    
│   ├── test_config.json                                                                                                            
│   ├── train.csv  # Data Split used for training                                                                                                          
│   └── valid.csv  # Data Split used for validation                                                                                                                 
├── inference                                                                                                                       
│   ├── config.py                                                                                                                   
│   ├── evaluate_model.py  # Script to test models on valid/test split                                                                                                       
│   └── test_online.py  # Script to run inference on models online                                                                                                  
├── model_snapshots.zip                                                                                                            
├── models                                                                                                                          
│   ├── dense_conv_variant.py                                                                                                       
│   ├── siamese_dense_conv.py                                                                                                       
│   └── siamese_dense_maxpool.py                                                                                                    
├── notebooks                                                                                                                       
│   ├── data-explore.ipynb                                                                                                          
│   ├── evaluate.ipynb                                                                                                              
│   ├── training-1.ipynb                                                                                                            
│   └── training.ipynb                                                                                                              
├── training                                                                                                                        
│   └── trainer.py                                                                                                                  
└── utils                                                                                                                           
    ├── data_generator.py                                                                                                           
    └── vocabulary.py
```

### Train/Validation Split

A 80/20 Train/Valid split has been done using stratified sampling on the column `is_duplicate`. The corresponding train/valid files are located at - `data/train.csv` and `data/valid.csv`

### Models

#### Embeddings

Pre-trained Glove embeddings are used to embed words into dense vectors. These word embeddings can accessed [here](http://nlp.stanford.edu/data/glove.42B.300d.zip).

#### NN Architectures

Three neural network architectures have been explored - 

1. `models/siamese_dense_maxpool.py`

	Siamese Style network with two streams for each sentence input. Each stream shares the same embedding matrix and a dense layer of size 400. The embedding matrix and dense layer encodes each word within the sentence. To encode the sentence itself maxpooling is done across the words to get a sentence encoding.

	Both streams are merged by concatenating the vectors and passing through a 3 layer MLP with one neuron on the final layer and sigmoid activation.

	Adam Optimizer with binary cross entropy loss is used to train the network.

	Let's call this network - `siamese-dense-maxpool`

2. `models/siamese_dense_conv.py`

	Two more streams are added to the network which use convolutional networks to extract temporal features within the sentence. Again, the weights of these convolutional streams are shared between the two input sentences. Global Max Pooling is performed after convolutions to extract a fixed size vector for the sentence.
	
	All four streams are then concatenated and passed through a series of Dense layers with final layer consisting of one neuron and sigmoid activation.
	
	Adam Optimizer with binary cross entropy loss is used to train the network.

	Let's call this network - `siamese-dense-conv`
	
3. `models/dense_conv_variant`

	This is the exact same model as 2. with the exception that no weight sharing is done between the input streams. Hence each of the Dense stream and Convolutional Stream has it's own set of weights.
	
	Let's call this network - `dense-conv`
	
#### Results

Below are the results recorded on the validation set.

| Model       | Accuracy        
| ------------- |:-------------:|
| dense-conv      | 0.823|
| siamese-dense-maxpool      | 0.822      |
| siamese-dense-conv | **0.8365**      |


#### Model Snapshots

A snapshot of the best performing model is given in `models_snapshots.zip` file. Pickled vocabulary is also provided.


#### Testing

Two scripts for evaluation are provided - 

1. `inference/evaluate_model.py` - Python script to evalate a model on a valid/test set. It needs a config file as input which sets the path of the model and vocabulary, points to valid/test csv file and optionally let's you set a threshold for converting predicted probabilities to classes. A sample config file is provided at `data/eval_config.json`. Run the script from parent directory as - 

	`python inference/evaluate_model.py -c data/eval_config.json`
	
	If the threshold is set to 0 in the config file, it assumes that you are searching for the best threshold and hence looks for the best threshold and outputs it. If not, it uses the threshold to calculate the accuracy on the given test/valid set.
	
	Assumption: Test set csv also has the same schema as the original data file provided.
	
2. `inference/test_online.py` - Python script to test the model on custom input. The script prompts the user for two sentences to be entered when the script is run. This script also needs a config file as argument which sets the model and vocabulary path. A sample config file is provided at `data/test_config.json`.

	Run the script from the parent directory as - 
	
	`python inference/test_online.py -c data/test_config.json`
	
	
## Possible Improvements

1. LSTM based models would definitely boost the accuracy. Ideally, another stream which encodes the sentences using an LSTM as well in addition to the Dense and Conv streams. Currently, these were omitted because of the time constraint as these models would have taken much longer time to train.

2. Approaches using attention mechanism can be explored which would attend to particular parts of segments in while looking at both the sentences together.

3. Adding custom distance functions to measure the similarity of encodings of each sentence and optimizing the network based on those distances themselves. For eg - Cosine Similarity / Euclidean Distance.

4. Simple Data Augmentation techniques like swapping some words with their synonyms from WordNet.